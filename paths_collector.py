from session import get_h
root_path='/root/'
root_session='/root/session_broker/'
system_yaml='/root/enclouden/dev/stack_orchestrator/configuration/system.yaml'
passwords_yaml='/root/enclouden/dev/stack_orchestrator/configuration/passwords.yaml'
system_network='/root/enclouden/dev/stack_orchestrator/configuration/system_network.yaml'
system_nodes='/root/enclouden/dev/stack_orchestrator/configuration/system_nodes/'
session='/var/lib/enclouden-session-broker/'
log_file='/var/log/enclouden/enclouden_session_broker_production_config.log'
ss='/var/lib/enclouden-session-broker/config/system.yaml'
sp='/var/lib/enclouden-session-broker/config/password.yaml'
sysbroker_sys='/var/lib/enclouden-session-broker/config/system.yaml'
sysbroker_pass='/var/lib/enclouden-session-broker/config/passwords.yaml'
rep='/var/lib/enclouden-session-broker/config/'
ch='/root/session_broker/session_broker/'
sshci='/var/lib/enclouden-session-broker/config/system.yaml'
broker='/root/enclouden/dev/stack_orchestrator/sessionscript/broker.sh'

def path_updater():
    with open(broker,'r') as fi:
        lines = fi.readlines()
    lines[7]='log_file='+log_file+'\n'
    lines[8]='ss='+ss+'\n'
    lines[9]='sp='+sp+'\n'
    lines[10]='session='+session+'\n'
    lines[11]='sysbroker_sys='+sysbroker_sys+'\n'
    lines[12]='sysbroker_pass='+sysbroker_pass+'\n'
    lines[13]='rep='+rep+'\n'
    lines[14]='sshci='+sshci+'\n'
    with open(broker, "w") as f:
    	f.writelines(lines)



path_updater()
