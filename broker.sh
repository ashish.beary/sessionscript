#!/bin/bash
node1=192.168.206.11
cloudip=192.168.206.10
dbip=192.168.206.9
bp=P37MV2QQCL51
dbp=3YEP9HSH19PJ
kap=KQGUYQTYZTPR
log_file=/var/log/enclouden/enclouden_session_broker_production_config.log
ss=/var/lib/enclouden-session-broker/config/system.yaml
sp=/var/lib/enclouden-session-broker/config/password.yaml
session=/var/lib/enclouden-session-broker/
sysbroker_sys=/var/lib/enclouden-session-broker/config/system.yaml
sysbroker_pass=/var/lib/enclouden-session-broker/config/passwords.yaml
rep=/var/lib/enclouden-session-broker/config/
sshci=/var/lib/enclouden-session-broker/config/system.yaml


node1=192.168.206.11
node2=192.168.206.202
node3=192.168.206.206





































deploy_poc() {
    cd /var/lib/enclouden-session-broker/
    mv config/system_poc.yaml config/system.yaml
    ask
    #c="$(awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' /etc/pki/libvirt-spice/ca-cert.pem)"
    #sed -i "s/certificate/$c/g" $sshci
    sed -i "s/productiondbip/$dbip/g" $sshci
    sed -i "s/productionopstackurl/$cloudip/g" $sshci
    sed -i "s/node1/$node1/g" $sshci
    sed -i "s/brkr/$bp/g" $sysbroker_pass
    sed -i "s/dta/$dbp/g" $sysbroker_pass
    sed -i "s/kap/$kap/g" $sysbroker_pass
    
    cd $session
    export FLASK_CONFIG=production_config
    python manage.py deploy

    chown apache:apache $log_file
    chmod 755 $log_file
    cp /var/lib/enclouden-session-broker/enclouden-session-broker.conf /etc/httpd/conf.d/
    apachectl graceful

    ssh root@$node1 'tmux new -s test -d'
    ssh root@$node1 'tmux new -s sessionbroker -d'
    ssh root@$node1 'tmux send-keys -t sessionbroker "cd /var/lib/enclouden-session-broker/" ENTER'
    ssh root@$node1 'tmux send-keys -t sessionbroker "export FLASK_CONFIG=production_config" ENTER'
    ssh root@$node1 'tmux send-keys -t sessionbroker "python manage.py run_celery --beat" ENTER'

}

install_poc() {
    cd /var/lib/enclouden-session-broker
    sh install.sh >> /root/.temp.txt
    x="$(cat /root/.temp.txt | grep -i 'error')"
    if [[ $? -eq 1 ]]
    then
        echo "install.sh successful"
        deploy_poc
    else
        echo "install.sh has not been successful, please fix the error and then run 'python session.py deploy_poc'"
        exit 1
    fi
}


hciclone() {
    rsync -azP --delete /var/lib/enclouden-session-broker/ root@$node2:/var/lib/enclouden-session-broker/ #node2
    rsync -azP --delete /var/lib/enclouden-session-broker/ root@$node3:/var/lib/enclouden-session-broker/ #node3
}

deploy_hci() {
    #c="$(awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' /etc/pki/libvirt-spice/ca-cert.pem)"
    #sed -i "43s/certificate/$c/g" $sshci

    sed -i "110s/productiondbip/$dbip/g" $sshci
    sed -i "124s/productionopstackurl/$cloudip/g" $sshci
    sed -i "148s/node1/$node1/g" $sshci
    sed -i "149s/node2/$node2/g" $sshci
    sed -i "150s/node3/$node3/g" $sshci
    sed -i "s/brkr/$bp/g" $sysbroker_pass
    sed -i "s/dta/$dbp/g" $sysbroker_pass
    sed -i "s/kap/$kap/g" $sysbroker_pass
    cd $session

    rsync -azP --delete /var/lib/enclouden-session-broker/config/ root@$node2:/var/lib/enclouden-session-broker/config/ #node2
    rsync -azP --delete /var/lib/enclouden-session-broker/config/ root@$node3:/var/lib/enclouden-session-broker/config/ #node3

    export FLASK_CONFIG=production_config
    python manage.py deploy
    ssh root@$node2 "export FLASK_CONFIG=production_config && cd /var/lib/enclouden-session-broker && python manage.py deploy && exit"
    ssh root@$node3 "export FLASK_CONFIG=production_config && cd /var/lib/enclouden-session-broker && python manage.py deploy && exit"

    chown apache:apache $log_file
    chmod 755 $log_file
    cp /var/lib/enclouden-session-broker/enclouden-session-broker.conf /etc/httpd/conf.d/

    ssh root@$node2 "chown apache:apache /var/log/enclouden/enclouden_session_broker_production_config.log && chmod 755 /var/log/enclouden/enclouden_session_broker_production_config.log && cp /var/lib/enclouden-session-broker/enclouden-session-broker.conf /etc/httpd/conf.d/ && exit"
    ssh root@$node3 "chown apache:apache /var/log/enclouden/enclouden_session_broker_production_config.log && chmod 755 /var/log/enclouden/enclouden_session_broker_production_config.log && cp /var/lib/enclouden-session-broker/enclouden-session-broker.conf /etc/httpd/conf.d/ && exit"

    apachectl graceful
    ssh root@$node2 "apachectl graceful && exit"
    ssh root@$node3 "apachectl graceful && exit"

    ssh root@$node1 'tmux new -s sessionbroker -d'
    ssh root@$node1 'tmux send-keys -t sessionbroker "cd /var/lib/enclouden-session-broker/" ENTER'
    ssh root@$node1 'tmux send-keys -t sessionbroker "export FLASK_CONFIG=production_config" ENTER'
    ssh root@$node1 'tmux send-keys -t sessionbroker "python manage.py run_celery --beat" ENTER'

    ssh root@$node2 'tmux new -s test -d'
    ssh root@$node2 'tmux new -s sessionbroker -d'
    ssh root@$node2 'tmux send-keys -t sessionbroker "cd /var/lib/enclouden-session-broker/" ENTER'
    ssh root@$node2 'tmux send-keys -t sessionbroker "export FLASK_CONFIG=production_config" ENTER'
    ssh root@$node2 'tmux send-keys -t sessionbroker "python manage.py run_celery" ENTER'
    
    ssh root@$node3 'tmux new -s test -d'
    ssh root@$node3 'tmux new -s sessionbroker -d'
    ssh root@$node3 'tmux send-keys -t sessionbroker "cd /var/lib/enclouden-session-broker/" ENTER'
    ssh root@$node3 'tmux send-keys -t sessionbroker "export FLASK_CONFIG=production_config" ENTER'
    ssh root@$node3 'tmux send-keys -t sessionbroker "python manage.py run_celery" ENTER'
}

install_hci() {
    echo "syncing files"
    cd /var/lib/enclouden-session-broker
    echo "running install.sh"
    sh install.sh > /root/.temp1 &
    echo "node1 done"
    ssh $node2 "cd /var/lib/enclouden-session-broker && sh install.sh > /root/.temp2 && exit" &
    echo "node2 done"
    ssh $node3 "cd /var/lib/enclouden-session-broker && sh install.sh > /root/.temp3 && exit" &
    echo "node3 done"
    wait
    ssh $node2 "scp /root/.temp2 root@$node1:/root/ && exit"
    ssh $node3 "scp /root/.temp3 root@$node1:/root/ && exit"
    echo
    echo "##################### Checking for errors in installation ########################"
    grep -i "failed\|error" /root/.temp1 /root/.temp2 /root/.temp3
    if [ $? -eq 1 ]
    then
        echo "install.sh successful"
        deploy_hci
    else
        echo "install.sh has not been successful, please fix the error and then run 'python sysadmin.py deploy_hci' on the FIRST NODE"
        exit 1
    fi
}


"$@"
