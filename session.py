import yaml,socket,os,sys,shutil,subprocess
import paths_collector


def get_h():
    with open(paths_collector.system_yaml,'r') as fi:
        f=yaml.load(fi)
        x=f['clusters']['hciclstr01']
        return len(x)

def poc_clone():
    node1=(socket.gethostbyname(socket.gethostname()))
    print("Enter password on prompt:")
    os.chdir(paths_collector.root_path)
    os.system("git clone https://ecndevops@bitbucket.org/enclouden/session_broker.git")
    os.system("git clone https://gitlab.com/kanishk.godha/sysbroker_config_files.git")
    os.chdir(paths_collector.root_session)
    os.system('git checkout ad_integration')
    if os.path.exists(paths_collector.session):
    	shutil.rmtree(paths_collector.session)
    os.makedirs(paths_collector.session)
    print("copying files")
    os.system('cp -r /root/session_broker/session_broker/* /var/lib/enclouden-session-broker/')
    os.system('echo "yes" | cp -r /root/sysbroker_config_files/sessionbroker_files/* /var/lib/enclouden-session-broker/config/')
    os.chdir('/root/enclouden/dev/stack_orchestrator/sessionscript/')
    print(node1)
    with open('broker.sh','r') as fi:
        lines = fi.readlines()
    lines[1]='node1='+node1+'\n'
    lines[17]='node1='+node1+'\n'
    with open("broker.sh", "w") as f:
        f.writelines(lines)  


def hci_clone():
    with open(paths_collector.system_yaml, 'r') as fi:
       f=yaml.load(fi)
       x=f['clusters']['hciclstr01']
       y=len(x)
       a = 0
       for h in x:
            with open('/root/enclouden/dev/stack_orchestrator/configuration/system_nodes/{0}.yaml'.format(h)) as nodefiles:
                    g=yaml.load(nodefiles)
                    z=g['network']['public']['ipaddr']
                    a = a + 1
                    exec("node%d = '%s'" % (a, z))

    poc_clone()
    with open('broker.sh','r') as fi:
        lines = fi.readlines()
    	lines[18]='node2='+node2+'\n'
    	lines[19]='node3='+node3+'\n'
    with open("broker.sh", "w") as f:
        f.writelines(lines)  
    os.system('sh broker.sh hciclone')   


def clone():
    t=get_h()
    if t==1:

        poc_clone()

    if t==3:
       
        hci_clone()   

def fetch_val():
    #nodeip=(socket.gethostbyname(socket.gethostname()))
    #nodeip=str(nodeip)
    with open(paths_collector.system_yaml,'r') as fi:
        f=yaml.load(fi)
        dbip=f['master_db_vip']
        dbip=str(dbip)
        cloudip=f['cloud_vip']
        cloudip=str(cloudip)
    with open(paths_collector.passwords_yaml,'r') as fi:
        f=yaml.load(fi)
        bp=f['rabbitmq_sessionbroker_pass']
        dbp=f['ecnsessionbroker_dbpass']
        kap=f['keystone_admin_pass']
    #l=["nodeip="+nodeip,"cloudip="+cloudip,"dbip="+dbip,"bp="+bp,"dbp="+dbp,"kap="+kap]
    with open('/root/enclouden/dev/stack_orchestrator/sessionscript/broker.sh','r') as fi:
        lines = fi.readlines()
        #lines[1]= 'nodeip='+nodeip+'\n'
        lines[2]='cloudip='+cloudip+'\n'
        lines[3]='dbip='+dbip+'\n'
        lines[4]='bp='+bp+'\n'
        lines[5]='dbp='+dbp+'\n'
        lines[6]='kap='+kap+'\n'
    with open("/root/enclouden/dev/stack_orchestrator/sessionscript/broker.sh", "w") as f:
        f.writelines(lines)


def deployment():
    q=get_h()
    print("running install.sh")
    if q==1:
        os.system('cd /root/enclouden/dev/stack_orchestrator/sessionscript/ && sh broker.sh install_poc')
    else:
        os.system('cd /root/enclouden/dev/stack_orchestrator/sessionscript/ && sh broker.sh install_hci')    
       

def deploy_hci():
    os.system('cd /root/enclouden/dev/stack_orchestrator/sessionscript/ && sh broker.sh deploy_hci')

def deploy_poc():
    os.system('cd /root/enclouden/dev/stack_orchestrator/sessionscript/ && sh broker.sh deploy_poc')

if __name__ == "__main__":
    calling='deploy'
    hci='run_celery'
    dpoc='deploy_poc'
    dhci='deploy_hci'
    if sys.argv[1] == calling : get_h() ; clone() ; fetch_val() ; deployment()
    elif sys.argv[1] == hci: nodes() ; run_celery()
    elif sys.argv[1] == dhci: deploy_hci()
    elif sys.argv[1] == dpoc: deploy_poc()
    else: sys.exit()
